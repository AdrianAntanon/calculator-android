package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextCalculator;
    private String digitsToCalculate = "", operator = "", remainingAmount = "";
    private final String PLUS = "+", MINUS = "-", MULTI = "*", DIV = "/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextCalculator = findViewById(R.id.editTextCalculator);
        Button buttonNum0 = findViewById(R.id.buttonCalculatorNum0);
        Button buttonNum1 = findViewById(R.id.buttonCalculatorNum1);
        Button buttonNum2 = findViewById(R.id.buttonCalculatorNum2);
        Button buttonNum3 = findViewById(R.id.buttonCalculatorNum3);
        Button buttonNum4 = findViewById(R.id.buttonCalculatorNum4);
        Button buttonNum5 = findViewById(R.id.buttonCalculatorNum5);
        Button buttonNum6 = findViewById(R.id.buttonCalculatorNum6);
        Button buttonNum7 = findViewById(R.id.buttonCalculatorNum7);
        Button buttonNum8 = findViewById(R.id.buttonCalculatorNum8);
        Button buttonNum9 = findViewById(R.id.buttonCalculatorNum9);
        Button buttonPlus = findViewById(R.id.buttonCalculatorPlus);
        Button buttonMinus = findViewById(R.id.buttonCalculatorMinus);
        Button buttonDiv = findViewById(R.id.buttonCalculatorDiv);
        Button buttonMulti = findViewById(R.id.buttonCalculatorMulti);
        Button buttonPoint = findViewById(R.id.buttonCalculatorPoint);
        Button buttonEqual = findViewById(R.id.buttonCalculatorEqual);
        Button buttonClear = findViewById(R.id.buttonCalculatorClear);

        buttonNum0.setOnClickListener(this);
        buttonNum1.setOnClickListener(this);
        buttonNum2.setOnClickListener(this);
        buttonNum3.setOnClickListener(this);
        buttonNum4.setOnClickListener(this);
        buttonNum5.setOnClickListener(this);
        buttonNum6.setOnClickListener(this);
        buttonNum7.setOnClickListener(this);
        buttonNum8.setOnClickListener(this);
        buttonNum9.setOnClickListener(this);
        buttonPlus.setOnClickListener(this);
        buttonMinus.setOnClickListener(this);
        buttonDiv.setOnClickListener(this);
        buttonMulti.setOnClickListener(this);
        buttonPoint.setOnClickListener(this);
        buttonEqual.setOnClickListener(this);
        buttonClear.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.buttonCalculatorNum0:
                digitsToCalculate = editTextCalculator.getText().toString();
                digitsToCalculate += "0";
                editTextCalculator.setText(digitsToCalculate);
                break;
            case R.id.buttonCalculatorNum1:
                digitsToCalculate = editTextCalculator.getText().toString();
                digitsToCalculate += "1";
                editTextCalculator.setText(digitsToCalculate);
                break;
            case R.id.buttonCalculatorNum2:
                digitsToCalculate = editTextCalculator.getText().toString();
                digitsToCalculate += "2";
                editTextCalculator.setText(digitsToCalculate);
                break;
            case R.id.buttonCalculatorNum3:
                digitsToCalculate = editTextCalculator.getText().toString();
                digitsToCalculate += "3";
                editTextCalculator.setText(digitsToCalculate);
                break;
            case R.id.buttonCalculatorNum4:
                digitsToCalculate = editTextCalculator.getText().toString();
                digitsToCalculate += "4";
                editTextCalculator.setText(digitsToCalculate);
                break;
            case R.id.buttonCalculatorNum5:
                digitsToCalculate = editTextCalculator.getText().toString();
                digitsToCalculate += "5";
                editTextCalculator.setText(digitsToCalculate);
                break;
            case R.id.buttonCalculatorNum6:
                digitsToCalculate = editTextCalculator.getText().toString();
                digitsToCalculate += "6";
                editTextCalculator.setText(digitsToCalculate);
                break;
            case R.id.buttonCalculatorNum7:
                digitsToCalculate = editTextCalculator.getText().toString();
                digitsToCalculate += "7";
                editTextCalculator.setText(digitsToCalculate);
                break;
            case R.id.buttonCalculatorNum8:
                digitsToCalculate = editTextCalculator.getText().toString();
                digitsToCalculate += "8";
                editTextCalculator.setText(digitsToCalculate);
                break;
            case R.id.buttonCalculatorNum9:
                digitsToCalculate = editTextCalculator.getText().toString();
                digitsToCalculate += "9";
                editTextCalculator.setText(digitsToCalculate);
                break;
            case R.id.buttonCalculatorPoint:
                digitsToCalculate = editTextCalculator.getText().toString();
                digitsToCalculate += ".";
                editTextCalculator.setText(digitsToCalculate);
                break;
            case R.id.buttonCalculatorPlus:
                remainingAmount = editTextCalculator.getText().toString();
                operator = "+";
                editTextCalculator.setText(null);
                break;
            case R.id.buttonCalculatorMinus:
                remainingAmount = editTextCalculator.getText().toString();
                operator = "-";
                editTextCalculator.setText(null);
                break;
            case R.id.buttonCalculatorMulti:
                remainingAmount = editTextCalculator.getText().toString();
                operator = "*";
                editTextCalculator.setText(null);
                break;
            case R.id.buttonCalculatorDiv:
                remainingAmount = editTextCalculator.getText().toString();
                operator = "/";
                editTextCalculator.setText(null);
                break;
            case R.id.buttonCalculatorClear:
                editTextCalculator.setText(null);
                digitsToCalculate = "";
                remainingAmount = "";
                break;
            case R.id.buttonCalculatorEqual:

                if (digitsToCalculate.isEmpty()){
                    Toast.makeText(MainActivity.this, "Introduzca el primer valor, por favor.", Toast.LENGTH_LONG).show();
                }else {
                    if (digitsToCalculate.length() < 1) {
                        Toast.makeText(MainActivity.this, "Introduzca el primer valor, por favor.", Toast.LENGTH_LONG).show();
                    } else {
                        if (operator.equals(PLUS) || operator.equals(MINUS) || operator.equals(MULTI) || operator.equals(DIV)) {
                            calculator();
                        } else {
                            Toast.makeText(MainActivity.this, "No es posible realizar esa operación, especifique un operador.", Toast.LENGTH_LONG).show();
                        }
                    }
                }


                break;
        }
    }

    public void calculator() {

        if (digitsToCalculate.length() < 1){
            Toast.makeText(MainActivity.this, "Introduzca el primer valor, por favor.", Toast.LENGTH_LONG).show();
        }else {
            if (remainingAmount.length() < 1){
                Toast.makeText(MainActivity.this, "Introduzca el segundo valor, por favor.", Toast.LENGTH_LONG).show();
            }else {
                double amount = Double.parseDouble(remainingAmount), digitsToCalc = Double.parseDouble(digitsToCalculate), result;

                switch (operator) {
                    case PLUS:
                        result = amount + digitsToCalc;
                        editTextCalculator.setText(String.valueOf(result));
                        break;
                    case MINUS:
                        result = amount - digitsToCalc;
                        editTextCalculator.setText(String.valueOf(result));
                        break;
                    case MULTI:
                        result = amount * digitsToCalc;
                        editTextCalculator.setText(String.valueOf(result));
                        break;
                    case DIV:
                        result = amount / digitsToCalc;
                        editTextCalculator.setText(String.valueOf(result));
                        break;
                }
            }

        }


    }

}